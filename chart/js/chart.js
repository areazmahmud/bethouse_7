
window.onload = function () {

    var options = {
        animationEnabled: true,
        backgroundColor: "rgb(102,102,102)",
        axisY2: {
            title: "Runs",
            titleFontColor: "#fff",
            lineColor: "#fff",
            labelFontColor: "#fff",
            tickColor: "#fff",
            includeZero: false
        },
        axisX: {
            title: "Overs",
            titleFontColor: "#fff",
            lineColor: "#fff",
            labelFontColor: "#fff",
            tickColor: "#fff",
            includeZero: false
        },
        axisY: {
            titleFontColor: "#666666",
            lineColor: "#666666",
            labelFontColor: "#666666",
            tickColor: "#666666",
            includeZero: false
        },
        data: [{

            type: "spline",
            name: "Figueirense",
            titleFontColor: "#fff",
            showInLegend: true,
            //xValueFormatString: "MMM YYYY",
            yValueFormatString: "#,##0 Units",
            dataPoints: [
                { x: 05, y: 70 },
                { x: 13, y: 98 },
                { x: 20, y: 150 },
                { x: 25, y: 170 },
                { x: 30, y: 200 },
                { x: 40, y: 250 },
                { x: 50, y: 280}

            ]
        },
            {
                type: "spline",
                name: "Atletico Mineiro",
                axisYType: "secondary",
                showInLegend: true,
                //xValueFormatString: "MMM YYYY",
                yValueFormatString: "$#,##0.#",
                dataPoints: [
                    { x: 05, y: 70 },
                    { x: 12, y: 100 },
                    { x: 22, y: 150 },
                    { x: 26, y: 170 },
                    { x: 28, y: 172 },
                    { x: 38, y: 180 },
                    { x: 48, y: 200 }

                ]
            }]
    };
    $("#chartContainer").CanvasJSChart(options);

}
