/**
 * Created by A Reaz Mahmud on 3/4/2018.
 */

$("#flexiselDemo3").flexisel({
    visibleItems: 5,
    animationSpeed: 1000,
    autoPlay: true,
    autoPlaySpeed: 3000,
    pauseOnHover: true,
    enableResponsiveBreakpoints: true,
    responsiveBreakpoints: {
        portrait: {
            changePoint:480,
            visibleItems: 1
        },
        landscape: {
            changePoint:640,
            visibleItems: 2
        },
        tablet: {
            changePoint:768,
            visibleItems: 3
        }
    }
});
$("#flexiselDemo1").flexisel({
    visibleItems: 5,
    animationSpeed: 1000,
    autoPlay: true,
    autoPlaySpeed: 3000,
    pauseOnHover: true,
    enableResponsiveBreakpoints: true,
    responsiveBreakpoints: {
        portrait: {
            changePoint:480,
            visibleItems: 1
        },
        landscape: {
            changePoint:640,
            visibleItems: 2
        },
        tablet: {
            changePoint:768,
            visibleItems: 3
        }
    }
});



//End flexisel logo slide

//Sports Logo Scroll
$(document).ready(function () {
    $(".arrow-right").bind("click", function (event) {
        event.preventDefault();
        $(".vid-list-container").stop().animate({
            scrollLeft: "+=336"
        }, 750);
    });
    $(".arrow-left").bind("click", function (event) {
        event.preventDefault();
        $(".vid-list-container").stop().animate({
            scrollLeft: "-=336"
        }, 750);
    });
});




$("#slideshow > div:gt(0)").hide();
setInterval(function () {
    $('#slideshow > div:first')
        .fadeOut(1000)
        .next()
        .fadeIn(1000)
        .end()
        .appendTo('#slideshow');
}, 3000);

$(document).ready(function ($) {
    $('.horoscope_section li a').click(function (event) {
        event.preventDefault();
        var horo_id = $(this).attr('data-id');
        $('.horo_summary').removeClass('horo_active');
        $('#' + horo_id).addClass('horo_active');
    });
});


//Sports Logo Scroll TWO
$(document).ready(function () {
    $(".arrow-1-right").bind("click", function (event) {
        event.preventDefault();
        $(".vid-list-1-container-1").stop().animate({
            scrollLeft: "+=336"
        }, 750);
    });
    $(".arrow-1-left").bind("click", function (event) {
        event.preventDefault();
        $(".vid-list-1-container-2").stop().animate({
            scrollLeft: "-=336"
        }, 750);
    });
});


$("#slideshow > div:gt(0)").hide();
setInterval(function () {
    $('#slideshow > div:first')
        .fadeOut(1000)
        .next()
        .fadeIn(1000)
        .end()
        .appendTo('#slideshow');
}, 3000);

$(document).ready(function ($) {
    $('.horoscope_section-1 li a').click(function (event) {
        event.preventDefault();
        var horo_id = $(this).attr('data-id');
        $('.horo_summary-1').removeClass('horo_active-1');
        $('#' + horo_id).addClass('horo_active-1');
    });
});
