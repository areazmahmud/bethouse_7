/**
 * Created by A Reaz Mahmud on 3/4/2018.
 */

jQuery(window).scroll(function () {
    if(jQuery(this).scrollTop() > 100 ) {
        jQuery(".go-up").css("right","20px");
    }else {
        jQuery(".go-up").css("right","-60px");
    }
});
jQuery(".go-up").click(function(){
    jQuery("html,body").animate({scrollTop:0},500);
    return false;
});
