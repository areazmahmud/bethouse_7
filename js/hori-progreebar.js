/**
 * Created by A Reaz Mahmud on 3/5/2018.
 */


//Horizontal progressbar
$('#jq').LineProgressbar({
    percentage:90,
    radius: '1px',
    height: '6px',
});
$('#html').LineProgressbar({
    percentage:80,
    radius: '1px',
    height: '6px',
    fillBackgroundColor: '#DA4453'
});
$('#css').LineProgressbar({
    percentage:70,
    radius: '1px',
    height: '6px',
    fillBackgroundColor: '#E0C341'
});

$('#bm-1').LineProgressbar({
    percentage:55,
    radius: '1px',
    height: '5px',
    fillBackgroundColor: '#913ca0'
});
$('#bm-2').LineProgressbar({
    percentage:70,
    radius: '1px',
    height: '5px',
    fillBackgroundColor: '#ff6c00'
});

$('#bm-3').LineProgressbar({
    percentage:45,
    radius: '1px',
    height: '5px',
    fillBackgroundColor: '#913ca0'
});
$('#bm-4').LineProgressbar({
    percentage:62,
    radius: '1px',
    height: '5px',
    fillBackgroundColor: '#ff6c00'
});
$('#bm-5').LineProgressbar({
    percentage:45,
    radius: '1px',
    height: '5px',
    fillBackgroundColor: '#913ca0'
});
$('#bm-6').LineProgressbar({
    percentage:62,
    radius: '1px',
    height: '5px',
    fillBackgroundColor: '#ff6c00'
});
$('#bm-7').LineProgressbar({
    percentage:45,
    radius: '1px',
    height: '5px',
    fillBackgroundColor: '#913ca0'
});
$('#bm-8').LineProgressbar({
    percentage:62,
    radius: '1px',
    height: '5px',
    fillBackgroundColor: '#ff6c00'
});